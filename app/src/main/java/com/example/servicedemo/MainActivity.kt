package com.example.servicedemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnStartService.setOnClickListener(this)
        btnStopService.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnStartService -> {
                val foregroundIntent = Intent(this, ForegroundServiceExample::class.java)
                startService(foregroundIntent)
                Log.d("VIJAY----11", "START")
            }
            R.id.btnStopService -> {
                val foregroundIntent = Intent(this, ForegroundServiceExample::class.java)
                stopService(foregroundIntent)
                Log.d("VIJAY----22", "STOP")
            }
        }
    }


}